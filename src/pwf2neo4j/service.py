"""Main classes for pwf2neo4j"""
from __future__ import annotations
from typing import Optional, Dict, Any
from dataclasses import dataclass, field
import logging
import asyncio
import json
import functools

import neo4j  # type: ignore
from datastreamcorelib.pubsub import PubSubMessage, Subscription
from datastreamservicelib.service import SimpleService

LOGGER = logging.getLogger(__name__)


@dataclass
class PWFNeo4JService(SimpleService):
    """Subscribe to parsed packets, store info in Neo4J"""

    _neodrv: Optional[neo4j.Driver] = field(init=False, default=None)

    async def teardown(self) -> None:
        """Tear down"""
        if self._neodrv:
            self._neodrv.close()
        await super().teardown()

    def reload(self) -> None:
        """Load configs, restart sockets"""
        super().reload()
        if self._neodrv:
            self._neodrv.close()
        drv_kwargs = dict(self.config["neo4j"]["driver_kwargs"])
        drv_kwargs.update({"auth": tuple(self.config["neo4j"]["auth"])})
        self._neodrv = neo4j.GraphDatabase.driver(self.config["neo4j"]["uri"], **drv_kwargs)
        self.init_db_schema()

        sub = Subscription(
            self.config["pwfparser"]["sub_sockets"],
            b"pwfparsed",
            self.parsed_pkt_callback,
            decoder_class=PubSubMessage,
        )
        self.psmgr.subscribe(sub)

    def parsed_pkt_callback(self, sub: Subscription, msg: PubSubMessage) -> None:
        """Callback for the packet subscription"""
        _ = sub
        # Just pass it to async task to take care off
        LOGGER.debug("Got {}".format(msg))
        asyncio.create_task(self.save_parsed_pkt(msg))

    def init_db_schema(self) -> None:
        """Initialize db schema"""
        queries = (
            "CREATE CONSTRAINT ON (s:SSID) ASSERT s.name IS UNIQUE",
            "CREATE CONSTRAINT ON (s:MAC) ASSERT s.name IS UNIQUE",
            "CREATE CONSTRAINT ON (s:Node) ASSERT s.name IS UNIQUE",
            "CREATE CONSTRAINT ON (s:SeenProbe) ASSERT s.name IS UNIQUE",
        )
        for query in queries:
            self.execute_query(query, {})

    async def save_parsed_pkt(self, msg: PubSubMessage) -> None:
        """Do the actual save"""
        loop = asyncio.get_running_loop()
        # TODO: Make a proper message abstraction class
        parsed_data = json.loads(msg.dataparts[1])

        query: Optional[str] = None
        query_args: Dict[str, Any] = {
            "node": parsed_data["nodename"],
        }

        if parsed_data["type"] == "wifibeacon":
            query = """MERGE (s:SSID { name: $name})
            ON CREATE SET s.created = timestamp(), s.seen=timestamp()
            ON MATCH set s.seen=timestamp()
            MERGE (n:Node { name: $node})
            ON CREATE SET n.created = timestamp(), n.seen=timestamp()
            ON MATCH set n.seen=timestamp()
            MERGE (m:MAC { name: $addr})
            ON CREATE SET m.created = timestamp(), m.seen=timestamp()
            ON MATCH set m.seen=timestamp()
            MERGE (n)-[sb:SeenBeacon]->(s)
            ON CREATE SET sb.created = timestamp(), sb.seen=timestamp()
            ON MATCH set sb.seen=timestamp()
            MERGE (m)-[bb:Beacon]->(s)
            ON CREATE SET bb.created = timestamp(), bb.seen=timestamp()
            ON MATCH set bb.seen=timestamp()
            """
            query_args["name"] = parsed_data["ssid"]
            query_args["addr"] = parsed_data["mac"]

        if parsed_data["type"] == "wifiprobe":
            probename = "{}_{}_{}".format(
                parsed_data["nodename"], parsed_data["mac"].replace(":", ""), parsed_data["ssid"]
            )
            query = """MERGE (s:SSID { name: $ssid})
            ON CREATE SET s.created = timestamp(), s.seen=timestamp()
            ON MATCH set s.seen=timestamp()
            MERGE (m:MAC { name: $addr})
            ON CREATE SET m.created = timestamp(), m.seen=timestamp()
            ON MATCH set m.seen=timestamp()
            MERGE (m)-[p:Probe]->(s)
            ON CREATE SET p.created = timestamp(), p.seen=timestamp()
            ON MATCH set p.seen=timestamp()
            MERGE (n:Node { name: $node})
            ON CREATE SET n.created = timestamp(), n.seen=timestamp()
            ON MATCH set n.seen=timestamp()
            MERGE (sp:SeenProbe { name: $probename})
            ON CREATE SET n.created = timestamp(), n.seen=timestamp()
            ON MATCH set n.seen=timestamp()
            MERGE (n)-[spn:NodeSeenProbe]->(sp)
            ON CREATE SET spn.created = timestamp(), spn.seen=timestamp()
            ON MATCH set spn.seen=timestamp()
            MERGE (sp)-[spm:NodeSeenProbe]->(m)
            ON CREATE SET spm.created = timestamp(), spm.seen=timestamp()
            ON MATCH set spm.seen=timestamp()
            MERGE (sp)-[sps:NodeSeenProbe]->(s)
            ON CREATE SET sps.created = timestamp(), sps.seen=timestamp()
            ON MATCH set sps.seen=timestamp()
            """
            query_args["probename"] = probename
            query_args["ssid"] = parsed_data["ssid"]
            query_args["addr"] = parsed_data["mac"]

        if query is None:
            return
        await loop.run_in_executor(None, functools.partial(self.execute_query, query, query_args))

    def execute_query(self, query: str, query_args: Dict[str, Any]) -> Any:
        """Wrapper to session run usable by asyncio executor"""
        assert isinstance(self._neodrv, neo4j.Driver)
        with self._neodrv.session() as session:
            return session.run(query, query_args)
