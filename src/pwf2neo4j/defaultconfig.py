"""Default configuration file contents"""

# Remember to add tests for keys into test_pwf2neo4j.py
DEFAULT_CONFIG_STR = """
[zmq]
pub_sockets = ["ipc:///tmp/pwf2neo4j_pub.sock", "tcp://*:55408"]

[pwfparser]
sub_sockets = ["tcp://127.0.0.1:5066"]

[neo4j]
uri = "bolt://localhost:7687"
auth = [ "neo4j", "neo4j" ]
[neo4j.driver_kwargs]

""".lstrip()
