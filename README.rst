=========
pwf2neo4j
=========

Store PWF parsed packets in Neo4J

NOTE 2020-02-09: Neo4J 4.0 has a problem with their own Python library, use version 3.5
of the server.

Usage
-----

Preparations
^^^^^^^^^^^^

#. Install Poetry (see below)
#. Create virtualenv (see below)
#. Checkout this repo
#. Run "poetry install"
#. Run "pwf2neo4j --defaultconfig >config.toml" to generate default config
#. Edit config

Running
^^^^^^^

#. Start with "pwf2neo4j config.toml"
#. Profit !


Development
-----------

TLDR:

- Create and activate a Python 3.7 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.8` my_virtualenv

- Init your repo::

    git init
    git add .
    git commit -m 'Cookiecutter stubs'
    git push origin master

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    pre-commit install
    pre-commit run --all-files

- Ready to go.

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).

Running "pre-commit run --all-files" and "py.test -v" regularly during development and
especially before committing will save you some headache.
