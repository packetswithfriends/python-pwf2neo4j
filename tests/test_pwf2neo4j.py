"""Package level tests"""
import asyncio
from pathlib import Path
import tomlkit  # type: ignore

import pytest  # type: ignore


from pwf2neo4j import __version__
from pwf2neo4j.defaultconfig import DEFAULT_CONFIG_STR
from pwf2neo4j.service import PWFNeo4JService


def test_version() -> None:
    """Make sure version matches expected"""
    assert __version__ == "0.2.1"


def test_defaultconfig() -> None:
    """Test that the default config parses, remember to add any mandatory keys here"""
    parsed = tomlkit.parse(DEFAULT_CONFIG_STR)
    assert "zmq" in parsed
    assert "pub_sockets" in parsed["zmq"]


@pytest.mark.asyncio
async def test_service_starts_and_quits(tmpdir):  # type: ignore
    """Make sure the service starts and does not immediately die"""
    # Create empty config file
    configpath = Path(tmpdir.join("testconf.toml"))
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        fpntr.write(DEFAULT_CONFIG_STR)
    # Create service instance
    serv = PWFNeo4JService(configpath)
    # Put it as a task in the eventloop (instead of running it as blocking via run_until_complete)
    task = asyncio.create_task(serv.run())
    # Wait a moment and make sure it's still up
    await asyncio.sleep(2)
    assert not task.done()
    # Make sure we have default PUBlish socket
    assert serv.psmgr.default_pub_socket
    assert not serv.psmgr.default_pub_socket.closed
    # Make sure the config got loaded
    assert "zmq" in serv.config
    assert "pub_sockets" in serv.config["zmq"]

    # Tell the service to quit and check the task is done and exitcode is correct
    serv.quit()
    # TODO: How to make this wait smarter
    await asyncio.sleep(0.1)
    assert task.done()
    assert task.result() == 0
    assert serv.psmgr.default_pub_socket.closed
